from numpy import *
from scatterplot import *
import matplotlib.pyplot as plt
from scipy.signal import convolve
n_max = 50
fx, fy = data(sin, (0, 3*pi), n_max)
gx, gy = data(cos, (0, 3*pi), n_max)
fy = fy + ones(n_max,)
gy = gy + ones(n_max,)
size = 20
fig, (ax_f, ax_g, ax_full, ax_same, ax_valid, ax_overlap) = plt.subplots(6, 1, sharex=True)
#############################################################
print "Convolution:"
#convolve full
convo_full_Y = convolve(fy, gy)
convo_full_X, c = data(sin, (0,6*pi), convo_full_Y.shape[0])
convo_full_X = convo_full_X - (3.0*pi/2.0)
print "full shape: ", convo_full_X.shape[0], ',', convo_full_Y.shape[0]

#convolve same
convo_same_Y = convolve(fy, gy, 'same')
convo_same_X, c = data(sin, (0, 3*pi), convo_same_Y.shape[0])
print "##################"
print "same shape: ", convo_same_X.shape[0], ',', convo_same_Y.shape[0]

#convolve valid
convo_valid_Y = convolve(fx, fy, 'valid')
convo_valid_X = 3.0*pi/2.0
#print "valid shape: ", convo_valid_X.shape[0], ',', convo_valid_Y.shape[0]

#plot function f
ax_f.scatter(fx, fy, color = 'yellow', s = size)
ax_f.set_title('Function f(x)')
ax_f.margins(0, 0.5)

#plot function g
ax_g.scatter(gx, gy, color = 'orange', s = size, alpha = 0.3)
ax_g.set_title('Function g(x)')
ax_g.margins(0, 0.5)

#plot convo full
ax_full.plot(convo_full_X, convo_full_Y, 'r--')
ax_full.set_title('f*g \'full\' ')
ax_full.margins(0, 0.5)

#plot convo same
ax_same.plot(convo_same_X, convo_same_Y, 'gp')
ax_same.set_title('f*g \'same\'')
ax_same.margins(0, 0.5)
#plot convo valid
#plt.plot(convo_valid_X, convo_valid_Y, 'b--')

ax_valid.scatter(convo_valid_X, convo_valid_Y, color = 'green', s = 3*size)
ax_valid.set_title('f*g \'valid\'')
ax_valid.margins(0, 0.5)
#plot overlap
ax_overlap.scatter(fx, fy, color = 'red', s = size, alpha = 0.3)
ax_overlap.scatter(gx, gy, color = 'blue', s = size, alpha = 0.3)
ax_overlap.plot(convo_same_X, convo_same_Y/sum(gy), 'g+')
ax_overlap.set_title('f*g \'overlap\'')
ax_overlap.margins(0, 0.5)




plt.show()
