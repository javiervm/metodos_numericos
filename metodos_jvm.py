#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#
#
# Materia:	Métodos Númericos
#
#
#       @author Javier Vargas Martínez
#       @title minimos numeros representables en maquina 
#       @filename metodos_jvm.py
#       @version 0.1
#       
# Profesor:	Dan S. Diaz
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
import struct

#obtener el binario
def binary(num):
	return ''.join(bin(ord(c)).replace('0b', '').rjust(8, '0') for c in struct.pack('!d', num))


x = 1.0
for i in xrange(5000):
	if (1+x) == 1.0:
		#print '(x)_2 = ', binary(x)
		print 'x = ', x
		print 'x = 1 +  1/2^', i
		break
	x /= 2.0

print '2^(-1073) = ', 2**(-1073)
print '2^(-1074) = ', 2**(-1074)
print 'x = ', 1 + 2**(-50)
print '::::::::::::::::::::::::::::::'
print '::::::::::::::::::::::::::::::'
for i in xrange(50,55):
	x = 1 + 2**(-i)
	print 'x = ', repr(x)
	#print binary(x)
	print i
