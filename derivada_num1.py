from scatterplot import *
from numpy import *
import matplotlib.pyplot as plt
n = 100000

def derivada_uno(X, Y):
	Xd = X[:-1]
	Yd = zeros(Y.shape[0]-1,)
	for i in xrange(Yd.shape[0]):
		h = X[i+1] - X[i]
		Yd[i] = (Y[i+1] - Y[i])/h
	#plotit(Xd, Yd)
	return Xd, Yd

def derivada_dos(X, Y):
	Xd = X[1:-1]
	Yd = zeros(Y.shape[0]-2,)
	for i in xrange(Yd.shape[0]):
		h = (X[i+1] - X[i]) +  (X[i+2] - X[i+1])
		Yd[i] = (Y[i+2] - Y[i])/h
	return Xd, Yd



cosenoX, cosenoY = data(cos, (0,4*pi), n)
#plotit(cosenoX, cosenoY)
sinX, sinY = data(sin, (0,4*pi), n)

print "#############"

#	plotit(Xd, Yd)
dX1, dY1 = derivada_uno(sinX, sinY)
dX2, dY2 = derivada_dos(sinX, sinY)
plt.scatter(cosenoX, cosenoY, color='green', s = 20, alpha = 0.8)
plt.scatter(dX1, dY1, color='yellow', s = 20, alpha = 0.2)
plt.scatter(dX2, dY2, color='black', s = 20, alpha = 0.2)
plt.show()

