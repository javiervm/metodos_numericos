import matplotlib.pyplot as plt
from numpy import *


def data(f, (a,b), n):
	X, Y = zeros((n,)), zeros((n,))
	n -= 1
	h = (b-a)/n
	x = a
	for i in xrange(n ):
		Y[i] = f(x)
		X[i] = x
		x += h
	X[n], Y[n] = b, f(b)
	return X, Y

