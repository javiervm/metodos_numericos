from numpy import *
from scatterplot import *
import matplotlib.pyplot as plt
from scipy.signal import convolve
n_max = 50
fx, fy = data(sin, (0, 3*pi), n_max)
gx, gy = data(cos, (0, 3*pi), n_max)
#fy = fy + ones(n_max,)
#gy = gy + ones(n_max,)
size = 20

# Para generar multiples graficas
# sharex para compartir eje x
# sharey para compartir eje y
fig, (ax_f, ax_g, ax_full, ax_same, ax_overlap) = plt.subplots(5, 1, sharex=True)
#############################################################

#convolve full
convo_full_Y = convolve(fy, gy)
add_convo =  (3.0*pi*(2.0 + 1/n_max))/4
convo_full_X, c = data(sin, (0 - add_convo, 3.0*pi + add_convo), convo_full_Y.shape[0])

#convolve same
convo_same_Y = convolve(fy, gy, 'same')
convo_same_X, c = data(sin, (0, 3*pi), convo_same_Y.shape[0])

#convolve valid
convo_valid_Y = convolve(fx, fy, 'valid')
convo_valid_X = 3.0*pi/2.0

#plot function f
ax_f.scatter(fx, fy, color = 'yellow', s = size)
ax_f.set_title('Function f(x)')
ax_f.margins(0, 0.5)
ax_f.locator_params(axis = 'y', nbins=6)

#plot function g
ax_g.scatter(gx, gy, color = 'orange', s = size, alpha = 0.3)
ax_g.set_title('Function g(x)')
ax_g.margins(0, 0.5)
ax_g.locator_params(axis = 'y', nbins=6)

#plot convo full
ax_full.plot(convo_full_X, convo_full_Y, 'r--')
ax_full.set_title('f*g \'full\' ')
ax_full.margins(0, 0.5)
ax_full.locator_params(axis = 'y', nbins=6)

#plot convo same
ax_same.plot(convo_same_X, convo_same_Y, 'gp')
ax_same.set_title('f*g \'same\'')
ax_same.margins(0, 0.5)
ax_same.locator_params(axis = 'y', nbins=6)

#plot overlap
ax_overlap.scatter(fx, fy, color = 'red', s = size, alpha = 0.3)
ax_overlap.scatter(gx, gy, color = 'blue', s = size, alpha = 0.3)
ax_overlap.plot(convo_same_X, convo_same_Y/sum(fy), 'g+')
ax_overlap.set_title('f*g \'overlap\'')
ax_overlap.margins(0, 0.5)
ax_overlap.locator_params(axis = 'y', nbins=6)
ax_overlap.locator_params(axis = 'x', nbins=18)

fig.subplots_adjust(hspace = 0.4 )
fig.suptitle("Grafica convolucion de sin - cos")

plt.show()
