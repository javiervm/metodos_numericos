n	= 30
x	= 0.5
h 	= 1
emax	= 0
from numpy import *

for i in xrange(n):
	h = 0.25 * h
	y = (sin(x + h) - sin(x))/h
	error = abs(cos(x) - y)
	print "i: ", i, "\th: ", h, "\ty: ", y, "\terror: ", error
	if error > emax:
		emax = error
		imax = i

print "imax: ", imax, "\temax: ", emax
