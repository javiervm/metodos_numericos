#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#
#
#   Spline de segundo grado (Spline cuadratico)  
#   
#
#   @author Javier Vargas Martínez
#   @title Spline cuadratico
#   @filename spline2
#   @version 0.1
#        
#
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
from numpy import *
import matplotlib.pyplot as plt

a = array([(0, 8), (1, 12), (3, 2), (4, 6), (8, 0)], dtype = 'float64')
print a

x = a[:, 0]
y = a[:, 1]

print x
print ":::::::::::"
print y




def zetas(Y, T, n):
	Zetas = arange(n, dtype='float64')
	Zetas[0] = Y[0]
	for index in xrange(0, Zetas.shape[0] - 2):
		Zetas[index + 1] = -Zetas[index] + 2. * ( (Y[index + 1] - Y[index]) / (T[index + 1] - T[index]))
	return Zetas

Zetas = zetas(x, y, x.shape[0])
print "Z : ", Zetas


def splineQ(T, Y, Z, n, x):
	n = n-1
	for i in xrange(n - 1, 0, -1):
		if x - T[i] >= 0:
			break
		return ( (Z[i + 1] - Z[i]) / 2.*(T[i+1] - T[i]) ) * (x - T[i])**2 + (Z[i] * (x- T[i]) + Y[i])

plt.scatter(x, y, color = 'red', alpha = 0.5)
knots = arange(20) / 19. * 8.
for k in knots:
	plt.scatter(k, splineQ(x, y, Zetas, y.shape[0], k), color = 'blue', alpha = 0.6)

plt.show()
		
	
