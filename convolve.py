from numpy import *
from scatterplot import *
import matplotlib.pyplot as plt
from scipy.signal import convolve
n_max = 200
sinX, sinY = data(sin, (0, 2*pi), n_max)
cosX, cosY = data(cos, (0, 2*pi), n_max)
#sinY = sinY + ones(n_max + 1 ,)
#cosY = cosY + ones(n_max + 1,)

#############################################################
#convolve full
convoY = convolve(sinY, sinY)
convoX, c = data(sin, (0, 2*pi), convoY.shape[0]-1)
#print "full shape: ", convoX.shape[0], ',', convoY.shape[0]
#convolve valid
valy = convolve(sinY, sinY, 'same')
valx, c = data(sin, (0, 2*pi), valy.shape[0]-1)
print "##################"
print "valid shape: ", valx.shape[0], ',', valy.shape[0]
#convolve same
plt.title('convolution')

plt.scatter(sinX, sinY, color = 'green', s = 20)
plt.subplot()
plt.scatter(sinX, cosY, color = 'black', s = 20)
plt.plot(convoX, convoY, 'r--')
plt.plot(valx, valy, 'b--')
valy = convolve(sinY, sinY, 'valid')
valx, c = data(sin, (0, 2*pi), valy.shape[0]-1)
print "valid shape: ", valx.shape[0], ',', valy.shape[0]
plt.plot(valx, valy, 'b--')
plt.show()
