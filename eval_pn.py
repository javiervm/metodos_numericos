#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#
#
# Evaluacion de Polinomios 
#	eval(P(n))
#
#	%input	:	lista de coeficientes
#				 x^n, x^n-1, ..., x^1, x^0
#				{a_n, a_n-1, ..., a_1, a_0}
#	%output	:	evaluacion de polinomio
#
#       @author Javier Vargas Martínez
#       @title Evaluación de Polinomios
#       @filename eval_pn
#       @version 0.1
#       
#
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

def eval_pn(list, x):
	if list.__len__() == 0:
		raise ValueError("Lista de coeficientes vacia::\n\tSe necesita al menos un coeficiente")
	elif list.__len__() == 1 or x==0:
		return list[0]
	else:
		# Horner
		ev = list[0]
		for coef in list[1:]: # para todo coeficiente en lista
			ev = (ev * x) + coef
		return ev
	
