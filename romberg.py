import numpy as np
from matplotlib import pyplot as plt
import plotfun as fp

def romberg (f, (a,b), n):
	inicio = 1  
	h = (b - a)/2.
	r = np.zeros((n,n), dtype='float64')
	r[0][0] = (h) * (f(a) + f(b))
	for i in xrange(inicio, n):
		sum = 0.0
		for k in xrange(inicio, 2**(i), 2):
			sum += f(a + k*h)
		r[i][0] = (r[i-1][0]/2) + h*sum
		for j in xrange(inicio, i+1):
			r[i][j] = r[i][j-1] + (r[i][j-1] - r[i-1][j-1])/(4**j - 1.0)
		h /= 2.	
	print_triangulo(r)

def print_triangulo(matrix):
	n = 1
	for line in matrix:
		for i in range(n):
			print "{0:.15f}".format(line[i]), '\t',

		n += 1
		print ' '
		
def f(x):
    return 4. / (1.+ x**2.)


def data(f, (a,b), n):
    X, Y = zeros((n,)), zeros((n,))
    h = (b-a)/n
    x = a
    for i in xrange(n):
        print x
        Y[i] = f(x)
        X[i] = x
        x += h
    return X, Y

romberg(f, (0.,1.), 5)

