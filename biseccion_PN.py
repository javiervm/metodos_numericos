#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#
#
# Método de biseccion  
#   encontrar raiz de una funcion {P(n)} en un intervalo 
#	Por ahora solo funciona con polinomios
#   %input  :  
#		1.-	lista de coeficientes del polinomio
#                x^n, x^n-1, ..., x^1, x^0
#               {a_n, a_n-1, ..., a_1, a_0}
#		2.-	intervalo
#				(a, b)
#		3.-	Cantidad de iteraciones
#				n_max
#		4.- error maximo
#				epsilon
#				
#   %output :   aproximacion de la raiz con un error
#				no mayor a epsilon
#
#       @author Javier Vargas Martínez
#       @title Metodo Biseccion
#       @filename biseccion
#       @version 0.1
#       
#
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
from eval_pn import *
import numpy as np

def biseccion(list, interval, max_iters, epsilon):
	a, b = interval
	f_a, f_b = eval_pn(list, a) * 1.0, eval_pn(list, b) * 1.0
	if (np.sign(f_a) == 0):
		print "r = ", a, "f_a = ", f_a
		return a
	if (np.sign(f_b) == 0):
		print "r = ", b, "f_b = ", f_b
		return b
	if (np.sign(f_a) == np.sign(f_b)):
		raise ValueError("No existen raices en el intervalo")
	if (np.sign(f_a) * np.sign(f_b)) == -1:
		#		error = b-a
		error = b - a 
		for iter in range(max_iters):
			error /= 2.0	
			#c = (b+a)/2
			#f_c = eval_pn(list, c)
			c = a + error	
			f_c = eval_pn(list, c)*1.0	
			print "iter = ", iter, "\tc = ", c, "f_c = ", f_c, "error = ", error
			if abs(error) < epsilon:
				print "Error menor que epsilon\n r ≈ ", c, "f_c = ", f_c
				return c
			if (np.sign(f_a) * np.sign(f_c)) == -1:
				b = c
				f_b = f_c
			else:
				a = c
				f_a = f_c

