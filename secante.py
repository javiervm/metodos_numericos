#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#
#
# Método de la secante
#   %input  :  
#			   1.- funcion
#						fn
#			   2.- intervalo
#						(a, b)
#			   3.- Cantidad de iteraciones
#							   n_max
#			   4.- error maximo
#							   epsilon
#							   
#   %output :   aproximacion de la raiz con un error
#							   no mayor a epsilon
#
#	   @author Javier Vargas Martínez
#	   @title Metodo Biseccion
#	   @filename biseccion
#	   @version 0.1
#	   
#
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
import numpy as np

def switch(a, b):
	return b, a

def fun(x):
	return 3 + (x**3) * (1 + x**2)

def secante(fn, interval, max_iters, epsilon):
	a,b = interval
	f_a = fn(a)
	f_b = fn(b)
	# intercambiar si son mas grande	# intercambiar si son mas grandess
	if abs(f_a) > abs(f_b):
		a, b = b, a
		f_a, f_b = f_b, f_a

	print "a=", a, "f_a", f_a
	print "b=", b, "f_b", f_b

	for i in xrange(2, max_iters):
		if abs(f_a) > abs(f_b):
			a, b = b, a
			f_a, f_b = f_b, f_a

		d = (b-a)/(f_b-f_a)
		b = a
		f_b = f_a
		d = d*f_a
		if abs(d) < epsilon:
			print "Converge"
			return
		a = a-d
		f_a = fn(a)
		print "i = ", i, "a = ", a, "f_a = ", f_a

secante(fun, (0.0, 1.0), 100, 0.00000001)

