#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#
#
# Método de Newton-Raphson  
#   encontrar raiz de una funcion {P(n)} en un intervalo 
#	   Por ahora solo funciona con polinomios
#   %input  :  
#		1.-	lista de coeficientes del polinomio
#				x^n, x^n-1, ..., x^1, x^0
#				{a_n, a_n-1, ..., a_1, a_0}
#		2(*).-	lista de coeficientes de la primera
#			derivada del polinomio
#				f'(n)
#		3.-	Un valor de x
#		
#		4.-	Cantidad de iteraciones
#			   n_max
#		5.- error maximo
#				epsilon
#		6.- minimo delta para la derivada
#				delta
#		
#		2(*) si la entrada es un polimonio no se necesita la entrada
#			de la derivada. La derivada se calcula dentro del método
#
#   %output :   aproximacion de la raiz con un error
#				no mayor a epsilon, o cuando
#				la derivada sea menor que delta
#
#		@author Javier Vargas Martínez
#		@title Metodo Newton-Raphson
#		@filename newton-raphson 
#		@version 0.1
#	   
#
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

from eval_pn import *
from numpy import polyder, poly1d

def newton_rap(list, x, max_iters, epsilon, delta):
	
	d1_f = polyder(list) #derivada del polinomio
	print "f(x) =\n", poly1d(list)
	print "f'(x) =\n", poly1d(polyder(list))
 	f_x = eval_pn(list, x) * 1.0
	print "\n-----------------------------\n"
	print "n = 0\tx = ", x, "f(x) = ", f_x
	for iter in xrange(1, max_iters):
		d1_fx = eval_pn(d1_f, x) * 1.0
		#print "f(", x, ") = ", f_x
		#print "f'(", x, ") = ", d1_fx
		#		f'(x)  < delta
		if  abs(d1_fx) < delta:
			print "Derivada pequeña"
			return
		d = f_x/d1_fx
		#print "El error d = ", d
		x = x - d
		f_x = eval_pn(list, x) * 1.0
		print "n = ", iter, "\tx = ", x, "f(x) = ", f_x
		if abs(d) < epsilon:
			print "::::::::\nerror = ", abs(d), "\n:::::::::::::\n"
			print "Error menor que epsilon\n r ≈ ", x, "f(x) = ", f_x
			return

