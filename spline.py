#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#
#
#	Spline 1  
#	
#
#	@author Javier Vargas Martínez
#	@title Splines
#	@filename spline
#	@version 0.1
#		 
#
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
import numpy as np
import matplotlib.pyplot as plt
from getpoints import data
from numpy import pi

def printVec(vec):
	for x in vec:
		if x < 0.:
			print "{0:.9f}".format(x), '\t',
		else:
			print "{0:.10f}".format(x), '\t',
	print "\n"
	

def spline(n, T, Y, x):
	for i in xrange(n-2, -1, -1):
		if (x - T[i]) >= 0.:
			break
	return (Y[i] + (x - T[i]) * ( (Y[i+1] - Y[i]) / (T[i+1] - T[i]) ))

tics = 30
T, Y = data(np.sin, (0., 2*pi), tics)
printVec(T)
printVec(Y)
max_items = 50.
x_axis = np.arange(max_items) / (max_items - 1.) * 2. * pi
plt.scatter(T, Y, color = 'red', alpha = 0.8)

for item in x_axis:
	#print item, " - - ", spline(10, T, Y, item)
	plt.scatter(item, spline(tics, T, Y, item), color = 'blue', alpha = 0.5)
plt.show()

