from numpy import *
import matplotlib.pyplot as plt
#generador de datos [X][Y] en intervalo (a, b), con n particiones
def data(fn, intervalo, n):
	a, b = intervalo
	h = (b - a)/n
	X = zeros((n,))
	FX = zeros((n,))
	for i in xrange(n):
		X[i] = a + i * h
		FX[i] = fn(a + i * h)
	return X, FX

X_proof, Y_proof = data(sin, (0, 2 * pi), 50)
#codigo para apartir de dos vectores de datos plotear en ventana
def plotit(X = X_proof, Y = Y_proof):
	plt.scatter(X, Y)
	plt.show()
