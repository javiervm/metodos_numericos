#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#
#
# Método de Newton-Raphson  
#   encontrar raiz de una funcion {P(n)} en un intervalo 
#	   Por ahora solo funciona con polinomios
#   %input  :  
#		1.-	lista de coeficientes del polinomio
#				x^n, x^n-1, ..., x^1, x^0
#				{a_n, a_n-1, ..., a_1, a_0}
#		2(*).-	lista de coeficientes de la primera
#			derivada del polinomio
#				f'(n)
#		3.-	Un valor de x
#		
#		4.-	Cantidad de iteraciones
#			   n_max
#		5.- error maximo
#				epsilon
#		6.- minimo delta para la derivada
#				delta
#		
#		2(*) si la entrada es un polimonio no se necesita la entrada
#			de la derivada. La derivada se calcula dentro del método
#
#   %output :   aproximacion de la raiz con un error
#				no mayor a epsilon, o cuando
#				la derivada sea menor que delta
#
#		@author Javier Vargas Martínez
#		@title Metodo Newton-Raphson
#		@filename newton-raphson 
#		@version 0.1
#	   
#
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

from eval_pn import *
from numpy import polyder, poly1d
import numpy as np

def fx(x):
	return np.cos(x) - np.cos(3.0*x)

def dfx(x):
	return 3.0*np.sin(3.0*x) - np.sin(x)

def newton_rap(fun, dfun, x, max_iters, epsilon, delta):
	
 	f_x =  fun(x) * 1.0
	for iter in xrange(1, max_iters):
		d1_fx = dfun(x) * 1.0
		if  abs(d1_fx) < delta:
			print "Derivada pequeña"
			return
		d = f_x/d1_fx
		#print "El error d = ", d
		x = x - d
		f_x = fun(x) * 1.0
		print "n = ", iter, "\tx = ", x, "f(x) = ", f_x
		if abs(d) < epsilon:
			print "::::::::\nerror = ", abs(d), "\n:::::::::::::\n"
			print "Error menor que epsilon\n r ≈ ", x, "f(x) = ", f_x
			return

