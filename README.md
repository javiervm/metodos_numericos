# README #

Métodos Numéricos


### What is this repository for? ###

* Implementación de múltiples métodos numéricos 
* 0.2
* Horner
* Bisección
* Newton - Raphson
* Derivadas Numéricas
* Splines (Lineal y cuadrático)

### Requirements ###

* Python [2.7]
* NumPy [1.9.3]
* SciPy [0.17.0]
* Matplotlib [1.5.1]

### Who do I talk to? ###
 mail: javama91@gmail.com