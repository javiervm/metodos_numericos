import matplotlib.pyplot as plt
from numpy import *


def f(x):
	return 4. / 1.+x**2.


def data(f, (a,b), n):
	X, Y = zeros((n,)), zeros((n,))
	h = (b-a)/n
	x = a
	
	for i in xrange(n):
		print x
		Y[i] = f(x)
		X[i] = x
		x += h
	return X, Y


#X, Y = data(f, (0.,1.), 100)
#plt.plot(X, Y) 

#plt.show()
